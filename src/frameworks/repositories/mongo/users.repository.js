const mongoose = require('mongoose');

const entityName = "User";

const {
    schemas: {
        user: userSchema
    }
} = require('../../database/mongo');

const repository = () => {
    //Schema
    const User = mongoose.model(entityName,userSchema);

    //crud executables
    return {
        add: async user => {
            const mongoObject = new User(user);

            return mongoObject.save();
        },
        update: async user => {
            const {
                id
            } = user;

            return User.findByIdAndUpdate(id,{
                ...user
            },{
                new: true
            }).lean();
        },
        delete: async id => {
            
            return User.deleteOne({ _id: id });
        },
        getByAccountNumber: async accountNumber => {
            
            return  User.findOne({ accountNumber: accountNumber });
        },
        getByIdentityNumber: async identityNumber => {
            
            return  User.findOne({ identityNumber: identityNumber });
        },
        getById: async id => {
            return User.findById(id);
        },
        getUserAll: async () => {
            return User.find();
        }
    }
}

module.exports = repository();