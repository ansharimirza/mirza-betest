const mongoose = require('mongoose');

const {
    Schema
} = mongoose;

module.exports = new Schema({
    userName: {
        type: String,
        required: true,
        unique: true 
    },
    accountNumber: {
        type: Number,
        required: true,
        unique: true,
        index: true
    },
    emailAddress: {
        type: String,
        required: true,
        unique: true, 
        lowercase: true,
        trim: true,
        match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, 'Please fill a valid email address']
    },
    identityNumber: {
        type: Number,
        required: true,
        unique: true,
        index: true
    }
});