const { createClient } = require("redis");

let redisClient;

module.exports = {
    connect: async () => {
        if (!redisClient) {
            let redisURL = process.env.REDIS_URI;
            if (redisURL) {
                redisClient = createClient({ url: redisURL, password: "secret" });

                try {
                    await redisClient.connect();
                    console.log(`Connected to Redis successfully!`);
                } catch (e) {
                    console.error(`Connection to Redis failed with error:`);
                    console.error(e);
                }

                redisClient.on("error", (e) => {
                    console.error(`Failed to create the Redis client with error:`);
                    console.error(e);
                });
            } else {
                throw new Error('REDIS_URI is not defined');
            }
        }
        return redisClient;
    },
    getClientRedis: async () => {
        return redisClient;
    }
};
