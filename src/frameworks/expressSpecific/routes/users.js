const express = require('express');

const {
    userControllers
} = require('../../../controllers');

const {
    JWT,
} = require('../../middleware');

module.exports = dependencies =>{
    const router  = express.Router();

    const {
        createUserController,
        updateUserController,
        deleteUserController,
        getAccountNumberController,
        getIdentityNumberController,
        getUserByIdController,
        getUserAllController,
        getJWTTokenController
    } = userControllers(dependencies);

    router.route('/token/').get(getJWTTokenController);
    router.route('/').post(JWT.verifyToken, createUserController).delete(JWT.verifyToken, deleteUserController).put(JWT.verifyToken, updateUserController);
    router.route('/:id').delete(JWT.verifyToken, deleteUserController);
    router.route('/account/:accountNumber').get(JWT.verifyToken, getAccountNumberController);
    router.route('/identity/:identityNumber').get(JWT.verifyToken, getIdentityNumberController);
    router.route('/:id').get(JWT.verifyToken, getUserByIdController);
    router.route('/').get(JWT.verifyToken, getUserAllController);
    
    

    return router;
}