const {
    Response,
    ResponseError,
} = require('../common');

module.exports = (err,req,res,next) => {

    if (err.result === undefined || err.statusCode === undefined) {
        res.status(422);
        res.json(new ResponseError({
            statusMessage: err.message || 'No MSG',
            statusDescription: err.statusDescription || 'failed',
        }));
    }
    
    res.json(new Response({
        statusCode: err.statusCode || 500,
        statusMessage: err.statusMessage || 'No MSG',
        statusDescription: err.statusDescription || 'Somebody failed',
        result: err.result
    }));
}