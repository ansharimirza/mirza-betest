module.exports.Response = class Response {
    constructor({
        statusMessage = null,
        result = [],
    }){
        this.statusMessage = statusMessage;
        this.result = result;
    }
}

module.exports.ResponseError = class ResponseError {
    constructor({
        statusMessage = null,
        statusDescription = null,
    }){
        this.statusMessage = statusMessage;
        this.statusDescription = statusDescription;
    }
}

