const createUserController = require('./createUser.controller');
const updateUserController = require('./updateUser.controller');
const deleteUserController = require('./deleteUser.controller');
const getAccountNumberController = require('./getAccountNumber.controller');
const getIdentityNumberController = require('./getIdentityNumber.controller');
const getUserByIdController = require('./getUserById.controller');
const getUserAllController = require('./getUserAll.controller');
const getJWTTokenController = require('./getJWTToken.controller');

module.exports = dependencies => {
    return {
        createUserController: createUserController(dependencies),
        updateUserController: updateUserController(dependencies),
        deleteUserController: deleteUserController(dependencies),
        getAccountNumberController: getAccountNumberController(dependencies),
        getIdentityNumberController: getIdentityNumberController(dependencies),
        getUserByIdController: getUserByIdController(dependencies),
        getUserAllController: getUserAllController(dependencies),
        getJWTTokenController: getJWTTokenController(dependencies),
    }
}