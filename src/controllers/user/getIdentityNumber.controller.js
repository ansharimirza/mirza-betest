const {
    Response
} = require('../../frameworks/common');

module.exports = dependencies => {
    const {
        useCases: {
            user: {
                getIdentityNumberUseCase
            }
        }
    } = dependencies;

    const getIdentityNumber = async (req,res,next)  => {
        try {
            const {
                identityNumber,
            } = req.params;

            const response = await getIdentityNumberUseCase(dependencies).execute(identityNumber);

            const statusCode = 200;
            res.status(statusCode);
            res.json(new Response({
                statusMessage: "",
                result: response
            }));

            next();

        } catch (error) {
            next(error);
        }
    }

    return getIdentityNumber;
}