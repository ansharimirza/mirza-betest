const {
    Response
} = require('../../frameworks/common');

module.exports = dependencies => {
    const {
        useCases: {
            user: {
                getAccountNumberUseCase
            }
        }
    } = dependencies;

    const getAccountNumber = async (req,res,next)  => {
        try {
            const {
                accountNumber,
            } = req.params;

            const response = await getAccountNumberUseCase(dependencies).execute(accountNumber);

            const statusCode = 200;
            res.status(statusCode);
            res.json(new Response({
                statusMessage: "",
                result: response
            }));

            next();

        } catch (error) {
            next(error);
        }
    }

    return getAccountNumber;
}