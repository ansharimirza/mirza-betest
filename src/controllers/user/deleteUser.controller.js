const {Response} = require('../../frameworks/common');

module.exports = dependencies => {
    const {
        useCases: {
            user: {
                deleteUserUseCase
            }
        }
    } = dependencies;

    const deleteUser = async (req,res,next)  => {
        try {

            const {
                id,
            } = req.params;

            const deleteUser = await deleteUserUseCase(dependencies);
            const response = await deleteUser.execute(id);

            const statusCode = 204;
            res.status(statusCode);
            res.json(new Response({
                statusMessage: "Resource deleted",
                result: response
            }));

            next();

        } catch (error) {
            next(error);
        }
    }

    return deleteUser;
}