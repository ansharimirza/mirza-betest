const {
    Response
} = require('../../frameworks/common');

module.exports = dependencies => {
    const {
        useCases: {
            user: {
                getUserByIdUseCase
            }
        }
    } = dependencies;

    const getUserById = async (req,res,next)  => {
        try {
            const {
                id,
            } = req.params;

            const response = await getUserByIdUseCase(dependencies).execute(id);

            const statusCode = 200;
            res.status(statusCode);
            res.json(new Response({
                statusMessage: "",
                result: response
            }));

            next();

        } catch (error) {
            next(error);
        }
    }

    return getUserById;
}