const {
    Response
} = require('../../frameworks/common');

module.exports = dependencies => {
    const {
        useCases: {
            user: {
                createUserUseCase
            }
        }
    } = dependencies;

    const createUser = async (req,res,next)  => {
        try {
            const {
                userName,
                accountNumber,
                emailAddress,
                identityNumber
            } = req.body;

            const createUser = await createUserUseCase(dependencies);
            const response = await createUser.execute({
                userName,
                accountNumber,
                emailAddress,
                identityNumber
            });

            const statusCode = 201;
            res.status(statusCode);
            res.json(new Response({
                statusMessage: "Resource created",
                result: response
            }));

            next();

        } catch (error) {
            next(error);
        }
    }

    return createUser;
}