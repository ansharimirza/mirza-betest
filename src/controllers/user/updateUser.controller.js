const {
    Response
} = require('../../frameworks/common');

module.exports = dependencies => {
    const {
        useCases: {
            user: {
                updateUserUseCase
            }
        }
    } = dependencies;

    const updateUser = async (req,res,next)  => {
        try {
            const {
                id,
                userName,
                accountNumber,
                emailAddress,
                identityNumber
            } = req.body;

            const updateUser = await updateUserUseCase(dependencies);
            const response = await updateUser.execute({
                user : {
                    id,
                    userName,
                    accountNumber,
                    emailAddress,
                    identityNumber
                }
            });

            const statusCode = 200;
            res.status(statusCode);
            res.json(new Response({
                statusMessage: "Resource updated",
                result: response
            }));

            next();

        } catch (error) {
            next(error);
        }
    }

    return updateUser;
}