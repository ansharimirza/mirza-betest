const {
    Response
} = require('../../frameworks/common');

module.exports = dependencies => {
    const {
        useCases: {
            user: {
                getUserAllUseCase
            }
        }
    } = dependencies;

    const getUserAll = async (req,res,next)  => {
        try {
            
            const response = await getUserAllUseCase(dependencies).execute();

            const statusCode = 200;
            res.status(statusCode);
            res.json(new Response({
                statusMessage: "",
                result: response
            }));

            next();

        } catch (error) {
            next(error);
        }
    }

    return getUserAll;
}