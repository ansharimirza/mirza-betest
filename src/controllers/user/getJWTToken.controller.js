const {
    Response
} = require('../../frameworks/common');

module.exports = dependencies => {
    const {
        useCases: {
            user: {
                getTokenUseCase
            }
        }
    } = dependencies;

    const getToken = async (req,res,next)  => {
        try {

            const response = await getTokenUseCase(dependencies).execute();

            const statusCode = 200;
            res.status(statusCode);
            res.json(new Response({
                statusMessage: "Resource created",
                result: response
            }));

            next();

        } catch (error) {
            next(error);
        }
    }

    return getToken;
}