const useCases = require('../useCases');
const repositories = require('../frameworks/repositories/mongo');
const { getClientRedis } = require('../frameworks/database/redis');

module.exports = {
    useCases,
    ...repositories,
    getClientRedis
};
