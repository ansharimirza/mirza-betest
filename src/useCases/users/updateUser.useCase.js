module.exports = dependencies => {
    const { usersRepository,getClientRedis } = dependencies;

    if (!usersRepository) {
        throw new Error('The user repository should exist in dependencies');
    }

    const execute = async ({ user = {} }) => {
        
        const userExist = await usersRepository.getById(user.id);
        
        // Assuming getById returns null if the user doesn't exist
        if (!userExist) {
            throw new Error('User does not exist');
        }

        const update = usersRepository.update(user); 
        const redisClient = await getClientRedis();

        await redisClient.del('users');
        return update; 
    };

    return {
        execute
    };
};
