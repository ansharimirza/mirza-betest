require('dotenv').config();
const jwt = require('jsonwebtoken');


module.exports = dependencies => {
    const {usersRepository} = dependencies;

    if (!usersRepository) {
        throw new Error('The user repository should be exists in dependencies');
    }

    const execute = () => {

        const expiresIn = '1d';

        const token = jwt.sign({},process.env.ACCESS_TOKEN_SECRET,
        {
            expiresIn: expiresIn, 
        });

        
        let dataTokens = [];
        dataTokens.push({
            accessToken: token,
            token_type: "bearer",
            expires_in: expiresIn,
            headers: "Authorization"
        });


        return dataTokens;

    }

    return {
        execute
    }
}