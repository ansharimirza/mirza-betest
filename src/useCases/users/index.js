const createUserUseCase = require('./createUser.useCase');
const updateUserUseCase = require('./updateUser.useCase');
const deleteUserUseCase = require('./deleteUser.useCase');
const getAccountNumberUseCase = require('./getAccountNumber.useCase');
const getIdentityNumberUseCase = require('./getIdentityNumber.useCase');
const getUserByIdUseCase = require('./getUserById.useCase');
const getUserAllUseCase = require('./getUserAll.useCase');
const getTokenUseCase = require('./getToken.useCase');

module.exports = {
    createUserUseCase,
    updateUserUseCase,
    deleteUserUseCase,
    getAccountNumberUseCase,
    getIdentityNumberUseCase,
    getUserByIdUseCase,
    getUserAllUseCase,
    getTokenUseCase
}