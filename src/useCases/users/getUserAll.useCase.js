module.exports = dependencies => {
    const { usersRepository, getClientRedis } = dependencies; // Include connectToRedis in dependencies

    if (!usersRepository) {
        throw new Error('The user repository should exist in dependencies');
    }

    const execute = async () => {
        try {
            const redisClient = await getClientRedis();

            let users = await redisClient.get('users');
            
            if (!users) {
                users = await usersRepository.getUserAll();
                await redisClient.set('users', JSON.stringify(users));
            } else {
                users = JSON.parse(users);
            }

            return users;
        } catch (error) {
            throw new Error('Failed to retrieve users');
        }
    };

    return {
        execute
    };
};
