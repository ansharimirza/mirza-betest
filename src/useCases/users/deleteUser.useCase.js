
module.exports = dependencies => {
    const {usersRepository, getClientRedis} = dependencies;
    if (!usersRepository) {
        throw new Error('The user repository should be exists in dependencies');
    }

    const execute = async (id) => {
        
        const userExist = await usersRepository.getById(id);
        
        // Assuming getById returns null if the user doesn't exist
        if (!userExist) {
            throw new Error('User does not exist');
        }

        const destroy = usersRepository.delete(id);
        const redisClient = await getClientRedis();

        await redisClient.del('users');

        return destroy;
    }

    return {
        execute
    }
}

