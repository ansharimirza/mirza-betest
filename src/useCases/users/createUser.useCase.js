const {User} = require('../../entities');

module.exports = dependencies => {
    const {usersRepository, getClientRedis} = dependencies;

    if (!usersRepository) {
        throw new Error('The user repository should be exists in dependencies');
    }

    const execute = async ({
        userName,
        accountNumber,
        emailAddress,
        identityNumber
    }) => {
        const user = new User({
            userName,
            accountNumber,
            emailAddress,
            identityNumber
        });

        const add = usersRepository.add(user);
        const redisClient = await getClientRedis();

        await redisClient.del('users');
        return add;
    }

    return {
        execute
    }
}

