module.exports.User = class User {
    constructor({
        id, 
        userName = null,
        accountNumber = null,
        emailAddress = null,
        identityNumber = null,
    }) {
            this.id = id;
            this.userName = userName;
            this.accountNumber = accountNumber;
            this.emailAddress = emailAddress;
            this.identityNumber = identityNumber;
        }
}
