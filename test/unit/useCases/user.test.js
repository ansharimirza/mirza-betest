const { 
    user : {
        createUserUseCase,
        getUserByIdUseCase,
        updateUserUseCase,
        deleteUserUseCase,
        getAccountNumberUseCase,
        getIdentityNumberUseCase,
    }
} = require('../../../src/useCases');


const {
    User,
} = require('../../../src/entities');

const {
    v4:uuidv4
} = require('uuid');

const Chance = require('chance');

const chance = new Chance();

const accountNumber = chance.integer();
const identityNumber = chance.integer();

describe('User use case', () =>{
    const mockUserRepo = {
        add: jest.fn(async user => ({
            ...user,
            id:uuidv4()
        })),
        getById: jest.fn(async id => ({
            id,
            userName: chance.name(),
            accountNumber: chance.integer(),
            emailAddress: chance.email(),
            identityNumber: chance.integer(),
        })),
        update: jest.fn(async user => user),
        delete: jest.fn(async id => id),
        getByAccountNumber: jest.fn(async accountNumber => ({
            id:uuidv4(),
            userName: chance.name(),
            accountNumber,
            emailAddress: chance.email(),
            identityNumber: chance.integer(),
        })),
        getByIdentityNumber: jest.fn(async identityNumber => ({
            id:uuidv4(),
            userName: chance.name(),
            accountNumber: accountNumber,
            emailAddress: chance.email(),
            identityNumber,
        })),
    };

    const mockRedisClient = {
        del: jest.fn()
    };

    const dependencies = {
        usersRepository: mockUserRepo,
        getClientRedis: async () => mockRedisClient
    };

    describe('add user use case', () =>{
        test('User should be added', async () =>{
            // Create user data
            const testUserData = new User({
                userName: chance.name(),
                accountNumber: chance.integer(),
                emailAddress: chance.email(),
                identityNumber: chance.integer(),
            });

            // Add a user using the use case
            const addedUser = await createUserUseCase(dependencies).execute(testUserData);
            
            // Check the received data
            expect(addedUser).toBeDefined();
            expect(addedUser.id).toBeDefined();
            expect(addedUser.userName).toBe(testUserData.userName); // Change to userName
            expect(addedUser.accountNumber).toBe(testUserData.accountNumber); // Change to accountNumber
            expect(addedUser.emailAddress).toBe(testUserData.emailAddress); // Change to emailAddress
            expect(addedUser.identityNumber).toBe(testUserData.identityNumber); // Change to identityNumber

            // Check that the dependency called as expected
            const call = mockUserRepo.add.mock.calls[0][0];
            expect(call.id).toBeUndefined();
            expect(call.userName).toBe(testUserData.userName);
            expect(call.accountNumber).toBe(testUserData.accountNumber);
            expect(call.emailAddress).toBe(testUserData.emailAddress);
            expect(call.identityNumber).toBe(testUserData.identityNumber);
        })
    })

    describe('Update user use case',  () => {
        test('User should be updated', async () => {
            //create a user data
            const testData = new User({
                id: uuidv4(),
                userName: chance.name(),
                accountNumber: chance.integer(),
                emailAddress: chance.email(),
                identityNumber: chance.integer(),
            });

            //call update user
            const updatedUser = await updateUserUseCase(dependencies).execute({
                user: testData
            });

            //check the data
            expect(updatedUser).toEqual(testData);

            //check the result
            const expectedUser = mockUserRepo.update.mock.calls[0][0];

            //check the call
            expect(expectedUser).toEqual(testData);
        })
    })

    describe('Delete user use case', () => {
        test('User should be deleted', async () => {
            const userId = uuidv4(); // Generate user ID
            
            // Panggil use case untuk menghapus pengguna
            const deletedUser = await deleteUserUseCase(dependencies).execute({ id: userId });
            
            // Periksa data pengguna yang dihapus
            expect(deletedUser.id).toEqual(userId);
            
            // Periksa bahwa fungsi penghapusan di repository dipanggil dengan benar
            const expectedUserId = mockUserRepo.delete.mock.calls[0][0].id;
            
            // Periksa bahwa ID pengguna yang dihapus sesuai dengan yang diharapkan
            expect(expectedUserId).toEqual(userId);
        })
    })

    describe('Get user by id use case', () => {
        test('User should be retrieved by id', async () => {
            const userId = uuidv4(); // Generate user ID
            
            // Panggil use case untuk mengambil pengguna berdasarkan ID
            const retrievedUser = await getUserByIdUseCase(dependencies).execute({ id: userId });
            
            // Periksa data pengguna yang diambil
            expect(retrievedUser.id.id).toEqual(userId);
        })
    })

    describe('Get account number use case', () => {
        test('Account number should be retrieved', async () => {
            // Call the use case to retrieve the account number
            const retrievedAccount = await getAccountNumberUseCase(dependencies).execute({ accountNumber:accountNumber });
            // Check the retrieved account number
            expect(retrievedAccount.accountNumber.accountNumber).toEqual(accountNumber);
        })
    })

    describe('Get identify number use case', () => {
        test('Identity number should be retrieved', async () => {
            // Call the use case to retrieve the account number
            const retrievedIdentity = await getIdentityNumberUseCase(dependencies).execute({ identityNumber:identityNumber });
            // Check the retrieved account number
            expect(retrievedIdentity.identityNumber.identityNumber).toEqual(identityNumber);
        })
    })
    
    
    
    
    
});
